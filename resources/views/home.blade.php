@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Shopping Cart</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ url('cart') }}" method="POST">
                        {{ csrf_field() }}
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Kode</th>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <input type="Checkbox" value="on" name="cek[]">
                                        <input type="hidden" name="cek[]" value="off">
                                    </td>
                                    <td><input type="text" class="form-control border-input" value="A0001" readonly="true" name="kode[]"></td>
                                    <td><input type="text" class="form-control border-input" value="Small Bread" readonly="true" name="nama[]"></td>
                                    <td><input type="number" id="harga" class="form-control border-input" value="5000" readonly="true" name="harga[]"></td>
                                    <td><input type="number" class="form-control border-input" value="1" min="1" name="jumlah[]"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="Checkbox" value="on" name="cek[]">
                                        <input type="hidden" name="cek[]" value="off">
                                    </td>
                                    <td><input type="text" class="form-control border-input" value="A0002" readonly="true" name="kode[]"></td>
                                    <td><input type="text" class="form-control border-input" value="Long Bread" readonly="true" name="nama[]"></td>
                                    <td><input type="number" id="harga" class="form-control border-input" value="10000" readonly="true" name="harga[]"></td>
                                    <td><input type="number" class="form-control border-input" value="1" min="1" name="jumlah[]"></td>
                                </tr>
                            </tbody>
                        </table> 
                        <div align="right" style="margin-right: 15px;">
                            <button for="submit" class="btn btn-primary">Check Out</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
